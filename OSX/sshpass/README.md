# sshpass

This is tricky to install sshpass on OSX with homebrew due pkg is blacklisted:
```shell
Error: No available formula with the name "sshpass"
We won't add sshpass because it makes it too easy for novice SSH users
```

But this is possible to do manually
```shell
brew create https://sourceforge.net/projects/sshpass/files/sshpass/1.06/sshpass-1.06.tar.gz --force
brew install sshpass
```

Also current repo contains appropriate _formula_: `sshpass.rb` for installing sshpass.
```shell
brew install https://gitlab.com/0xck/misc/blob/master/OSX/sshpass/sshpass.rb
```
